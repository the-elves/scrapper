__author__ = 'illuminati'
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
driver = webdriver.Firefox()
driver.get("https://malwr.com/analysis")
print driver.title

def pressPageDown(element,driver):
    driver.execute_script("return arguments[0].scrollIntoView();", element)
    driver.execute_script("window.scrollBy(00,-200);");

def behaviouralAnalysisExists(driver):
    behaviouralAnalysisXPath = "//*[@id=\"graph_hook\"]"
    behaviouralAnalysisButton = driver.find_elements_by_xpath(behaviouralAnalysisXPath)
    if (len(behaviouralAnalysisButton) == 0):
        return False
    else:
        return True
def writeTableToFile( file, driver):
    string = ""
    i = 0;
    bPTableXpath = "//*[contains(@id,\"process_\")]/div[3]/table/tbody/tr"
    bPTable = driver.find_elements_by_xpath(bPTableXpath)
    nRows = len(bPTable)
    bPTableXpath = "//*[contains(@id,\"process_\")]/div[3]/table/tbody/tr/td"
    bPTable = driver.find_elements_by_xpath(bPTableXpath)
    print "nRows = " + str(nRows)
    while(i<nRows):
        j =0
        print "reading " + str(i) + "th row"
        while(j<6):
            bPTable = driver.find_elements_by_xpath(bPTableXpath)
            string = string + "," + bPTable[i*6+j].text
            #print "text"+str(j) +" = " + cell[j].text
            j= j + 1
        print string
        file.write(string)
        string = ""
        i= i + 1
    # bPTableXpath = "//*[contains(@id,\"process_\")]/div[3]/table/tbody"
    # bPTable = driver.find_elements_by_xpath(bPTableXpath)
    #file.write(string)

def findNumberOfPages(driver):
    pageULXPath = "//*[contains(@id,\"process_\")]/div[2]/ul"
    uL = driver.find_element_by_xpath(pageULXPath)
    pressPageDown(uL,driver)
    uList=uL.find_elements_by_tag_name("li")
    numberOfPages = uList[len(uList)-1].text
    print "number of pages =" + numberOfPages
    return int(numberOfPages)

def goToPage(i,driver):
    print "going to page " + str(i)
    pageULXPath = "//*[contains(@id,\"process_\")]/div[2]/ul"
    ul = driver.find_element_by_xpath(pageULXPath)
    pageXpath  = "./*[.=\""+str(i) +"\"]"
    a = ul.find_element_by_xpath(pageXpath)
    a = a.find_element_by_tag_name("a")
    a.click()
    pressPageDown(ul,driver)
    time.sleep(5)

def iterateOverBehaviourProfile(driver):
    malwareNameXPath = "//*[contains(@id,\"process_\")]/div[1]/b[1]"
    pressPageDown(driver.find_element_by_xpath(malwareNameXPath),driver)
    filename = driver.find_element_by_xpath(malwareNameXPath).text
    print "filename = " + filename
    file = open(filename,"w+")
    i = 1
    numberOfPages = findNumberOfPages(driver)
    while(i<=numberOfPages):
        writeTableToFile(file,driver)
        i = i + 1
        if(i>numberOfPages):
            file.close()
            break;
        goToPage(i,driver);


def goToBehaviouralAnalysis(driver):
    behaviouralAnalysisXPath = "//*[@id=\"graph_hook\"]"
    behaviouralAnalysisButton = driver.find_element_by_xpath(behaviouralAnalysisXPath)
    behaviouralAnalysisButton.click()
    iterateOverBehaviourProfile(driver);
    driver.back();

def iterateOverMalwares(driver):
    tableXpath = "/html/body/div[2]/div[3]/div[2]/table/tbody/tr"
    rows =  driver.find_elements_by_xpath(tableXpath)
    i = 0
    while( i < len(rows)):
        col = rows[i].find_elements_by_tag_name("td")[1]
        print col.text;
        malwareAnalysisLink = col.find_element_by_tag_name("a")
        malwareAnalysisLink.click();
        if (behaviouralAnalysisExists(driver)):
            goToBehaviouralAnalysis(driver);
        driver.back();
        rows =  driver.find_elements_by_xpath(tableXpath)
        i= i + 1;
        #goBack(driver);

nextButton = driver.find_elements_by_xpath("/html/body/div[2]/div[1]/ul")
while len(nextButton) != 0 :
    iterateOverMalwares(driver)
    nextButton = driver.find_elements_by_xpath("/html/body/div[2]/div[1]/ul")
    nextButton[0].click()
    break